# Classe responsável por fazer as ações básicas na Tela de Identificacao: (GET, SET, CLICK) e validações
# encoding: utf-8

module XP
  module Home
    class Login < SitePrism::Page

      @@input_cnpj = 'login.input_cnpj'
      @@input_senha = 'login.input_senha'
      @@link_meus_pedidos = 'login.link_meus_pedidos'

      def is_login_page
        WaitPage.new
        is_visible_el 'login.title'
      end

      def set_cnpj (cnpj)
        send_keys_el @@input_cnpj, cnpj
      end

      def set_senha (senha)
        send_keys_el @@input_senha, senha
      end

      def do_login_from_properties usuario, soft_login
        get_user usuario
        set_cnpj $usuario_cnpj
        tracing "Usuário sorteado: #{$usuario_cnpj}"
        set_senha $usuario_senha
        press_continue
      end

      def do_login(usuario, senha)
        $usuario_cnpj = usuario
        $usuario_senha = senha

        set_cnpj usuario
        set_senha senha

        press_continue
      end

      def close_frame
        if MOBILE
          is_frame_on = enter_frame_mobile 'login.social_miner'
          unless is_frame_on
            sleep 6
            enter_frame_mobile 'login.social_miner'
          end

          if is_visible_el 'login.frame_close'
            click_el_script 'login.frame_close'
          end
          exit_frame_mobile
        end
      end

      def press_continue
        click_el 'login.button_continuar'
      end

      def choose_new_user
        is_visible_el_desktop 'login.user_not_registered'
        click_el_desktop 'login.user_not_registered'
        begin
          click_el 'login.button_cadastrar'
        rescue StandardError
          if MOBILE
            close_frame
            if is_visible_el 'login.button_cadastrar'
              click_el 'login.button_cadastrar'
            end
          end
        end
      end

      def get_user usuario
        massa_cadastro = YAML.load_file('features/support/data/massa/cadastro.yml')
        user = usuario.to_s.gsub(/[\[\]\\\"]/, '')
        $usuario_cnpj = "#{massa_cadastro['login'][ENV['AMBIENTE']][user]['cnpj']}"
        $usuario_senha = "#{massa_cadastro['login'][ENV['AMBIENTE']][user]['senha']}"
      end

    end
  end
end