module XP
  module Home
    class Home < SitePrism::Page
      WaitPage.new
      @@url = "#{DOMINIO['xpRico'][ENV['AMBIENTE']]['home']}"
      @@links = YAML.load_file('features/support/data/config/links.yml')
      set_url @@url

      # load_validation {WaitPage.new}

      def login_access
          # is_visible_el_desktop 'home.button_login'
          click_el_desktop 'home.button_login'
       end

      def login_access_url
        url = "#{DOMINIO[ENV['xpRico']][ENV['AMBIENTE']]['carrinho']}""#{@@links['login']}"
        go_to url
      end

      def access_validation
        $current_host = Capybara.current_host.to_s
        $ip_host = IPSocket::getaddress(@@url.gsub("http://", ""))
        tracing 'Acessado site: ' + $current_host
        tracing 'Ip Site: ' + $ip_host
        #screenshot(@@input_busca, 'Acessada tela home')
      end

      def is_home_page
        is_visible_el @@input_busca
      end
    end
  end
end
