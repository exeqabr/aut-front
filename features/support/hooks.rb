require 'open-uri'
require 'prawn'
require 'capybara-screenshot/rspec'

SitePrism::Page.class_eval do
  include Commons, XP, ClickEl, SendKeys, Select, CheckVisibility, CheckData, FindEl, Alerts, Iframes, WaitElement, AssertAndPrint
end

SitePrism::Section.class_eval do
  include Commons, XP, ClickEl, SendKeys, Select, CheckVisibility, CheckData, FindEl, Alerts, Iframes, WaitElement, AssertAndPrint
end

=begin
Screenshots.class_eval do
  include Commons
end
=end

$pdf_names = []

Before do |scenario|

  Capybara.current_session.driver.browser.manage.delete_all_cookies
  $LOG = Logger.new('automation_log.log')
  $count_img_scenario =  0

  ## Create directory for saved screenshots
  #create_directory_evidences(scenario)
  $Scenario = scenario

  tracing '---- Executando Cenário: ' + scenario.name.to_s.center(5).upcase
  tracing 'Driver: ' + Capybara.current_driver.to_s

end


After do |scenario|

    scroll_page_up 300
    img = Capybara.page.save_screenshot "#{$file_path}/final_0.png"
    embed img, 'image/png'
    for i in 1..2 do
      img = Capybara.page.save_screenshot "#{$file_path}/final_#{i}.png"
      embed img, 'image/png'
      scroll_page_down 150
    end

  file_logger = Dir.pwd + '/' + $LOG.instance_variable_get(:@logdev).instance_variable_get(:@filename)
  $LOG.close
  FileUtils.mv(file_logger, $file_path, :force => true)

  unless $RAISE_ERROR.nil?
    unless $RAISE_ERROR.empty?
      puts $RAISE_ERROR
      $RAISE_ERROR = ""
    end
  end


end


at_exit do
  Capybara.current_session.driver.quit
end

