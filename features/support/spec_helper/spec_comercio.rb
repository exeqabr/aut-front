module XP
  def left_menu_mobile
    XP::Home::SectionLeftMenuMobile.new
  end

  def home
    XP::Home::Home.new
  end

  def product_detail
    XP::Home::ProductDetail.new
  end

  def account
    XP::Home::Account.new
  end

  def register
    XP::Home::Register.new
  end

  def login
    XP::Home::Login.new
  end

  def address
    XP::Home::Address.new
  end

  def cart
    XP::Home::Cart.new
  end

  def payment
    XP::Home::Payment.new
  end

  def payment_credit_card
    XP::Home::PaymentCreditCard.new
  end

  def payment_boleto
    XP::Home::PaymentBoleto.new
  end

  def payment_debito_online
    XP::Home::PaymentDebitoOnline.new
  end

  def finished_payment
    XP::Home::FinishedPayment.new
  end

  def orders
    XP::Home::Orders.new
  end

  def logist
    XP::Home::Logist.new
  end

end
