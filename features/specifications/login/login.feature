#language: pt
#encoding: utf-8
@FullSmoke
@Login

Funcionalidade: Fazer login

  - Narrativa: Fazer login nos sites

  Cenario: Acessar página home do site
    Dado que estou na tela inicial do site --HomePage
    E o botão de login está visivel --LoginPage
    #'Quando clico no botão de login --LoginPage
    #'Então confirmo que o usuário está logado --LoginPage

